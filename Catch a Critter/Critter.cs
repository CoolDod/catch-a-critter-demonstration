﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Catch_a_Critter
{
    class Critter
    {
        // VARIABLES
        Texture2D spr;          // Critter sprite
        Vector2 pos;            // Critter position
        Score scoreObj;         // Score object
        SoundEffect clickSound; // Click sound effect
        int value = 10;         // Score gained from critter
        bool visible;           // Whether the critter is visible or not

        // METHODS
        public Critter(Score scoreObject)
        {
            scoreObj = scoreObject;
        }

        // Spawn - Spawns the critter at a random position
        public void Spawn(GameWindow window)
        {
            // Get screen bounds
            var xMin = 0;
            var yMin = 0;
            var xMax = window.ClientBounds.Width - spr.Width;
            var yMax = window.ClientBounds.Height - spr.Height;

            // Create new rng
            Random rnd = new Random();

            // Choose and set random position
            pos = new Vector2(rnd.Next(xMin, xMax), rnd.Next(yMin, yMax));

            // Show critter
            visible = true;
        }


        // Despawn - Hides the critter
        public void Despawn()
        {
            // Hide critter
            visible = false;
        }


        // Input - Reads player input
        public void Input()
        {
            // Get current mouse state
            MouseState mouse = Mouse.GetState();

            // Create bounding box for critter
            Rectangle bbox = new Rectangle((int)pos.X, (int)pos.Y, spr.Width, spr.Height);

            // If critter is visible and left button is held down and mouse is over critter
            if (visible && mouse.LeftButton == ButtonState.Pressed && bbox.Contains(mouse.X, mouse.Y))
            {
                // Despawn the critter
                Despawn();

                // Add to score
                scoreObj.AddScore(value);

                // Play click sound
                clickSound.Play();
            }
        }


        // Load - Loads a texture for the critter
        public void Load(ContentManager content)
        {
            spr = content.Load<Texture2D>("graphics/sloth");
            clickSound = content.Load<SoundEffect>("audio/buttonclick");
        }


        // Draw - Draws the critter to the screen
        public void Draw(SpriteBatch batch)
        {
            // If critter is visible or not
            if (visible)
                batch.Draw(spr, pos, Color.White);
        }
    }
}
