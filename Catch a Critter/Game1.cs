﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Catch_a_Critter
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        const int CRITTER_NUM = 10;
        Critter[] critters = new Critter[CRITTER_NUM];    // Array of critters
        Score score;

        const float SPAWN_DELAY = 3f;   // Time between citter spawns
        float spawnTimer = SPAWN_DELAY; // Time until next critter spawn
        int critterInd = 0;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            // Create new score instance
            score = new Score();
            // Load the scores font
            score.Load(Content);

            // Load all critters
            for (var i = 0; i < CRITTER_NUM; ++i)
            {
                // Create new critter instance
                critters[i] = new Critter(score);
                // Load critter texture
                critters[i].Load(Content);
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            foreach (Critter c in critters)
                c.Input();

            // Update spawn timer
            spawnTimer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            // If spawn timer has run out
            if (spawnTimer <= 0f)
            {
                // Reset spawn timer
                spawnTimer = SPAWN_DELAY;

                // Spawn a new critter
                critters[critterInd].Spawn(Window);
                ++critterInd;
            }
            
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            // Draw critter
            foreach (Critter c in critters)
                c.Draw(spriteBatch);

            // Draw score
            score.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
