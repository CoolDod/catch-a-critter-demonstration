﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Catch_a_Critter
{
    class Score
    {
        // VARIABLES
        int value;
        Vector2 pos = new Vector2(10, 10);
        SpriteFont font;

        // METHODS
        // AddScore - Adds to the score value
        public void AddScore(int score)
        {
            value += score;
        }

        // Load - Loads the font for the score
        public void Load(ContentManager content)
        {
            font = content.Load<SpriteFont>("fonts/mainFont") ;
        }

        // Draw - Draws the score to the screen
        public void Draw(SpriteBatch batch)
        {
            batch.DrawString(font, "Score: "  + value.ToString(), pos, Color.White);
        }
    }
}
